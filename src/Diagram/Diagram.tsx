import go from 'gojs'
import { ReactDiagram } from 'gojs-react'
import { FC, useEffect, useRef } from 'react'
import { useStore } from '../store'
import { linkTemplateMap } from './linkTemplateMap'
import { nodeTemplateMap } from './nodeTemplateMap'
import { selectDiagramData } from './selectDiagramData'
import { DiagramModelData } from './types'

const $ = go.GraphObject.make

const treeOrderFn: go.TreeLayout['comparer'] = (a, b) =>
  (a.node?.data.index ?? 999) - (b.node?.data.index ?? 999)

const makeDiagram = () => {
  const diagram = $(go.Diagram, {
    allowClipboard: false,
    allowCopy: false,
    allowDelete: false,
    allowDragOut: false,
    allowDrop: false,
    allowGroup: false,
    allowHorizontalScroll: true,
    allowInsert: false,
    allowLink: false,
    allowMove: false,
    allowRelink: false,
    allowReshape: false,
    allowResize: false,
    allowRotate: false,
    allowSelect: false,
    allowTextEdit: false,
    allowUndo: false,
    allowUngroup: false,
    allowVerticalScroll: true,
    allowZoom: true,

    model: $(go.GraphLinksModel, { linkKeyProperty: 'key' }),
    nodeTemplateMap,
    linkTemplateMap,
  })

  const mainLayer = diagram.findLayer('') as go.Layer
  diagram.addLayerAfter($(go.Layer, { name: 'activatedEdge' }), mainLayer)

  diagram.toolManager.hoverDelay = 200

  diagram.addDiagramListener('BackgroundSingleClicked', () => {
    const state = diagram.model.modelData as DiagramModelData
    if (state.activation?.type === 'linking') state.cancelLinking()
    else state.activate(null)
  })

  let diagramNeedsLayout = false

  diagram.addModelChangedListener((e) => {
    switch (e.propertyName) {
      case 'category':
        diagramNeedsLayout = true
        break
      case 'CommittedTransaction':
        if (diagramNeedsLayout) {
          diagramNeedsLayout = false
          diagram.layoutDiagram(true)
        }
    }
  })

  return diagram
}

interface DiagramObjects {
  diagram: go.Diagram
  treeLayout: go.TreeLayout
  layeredLayout: go.LayeredDigraphLayout
}

const makeDiagramObjects = (): DiagramObjects => ({
  diagram: makeDiagram(),

  treeLayout: $(go.TreeLayout, {
    angle: 90,
    layerSpacing: 32,
    sorting: go.TreeLayout.SortingAscending,
    comparer: treeOrderFn,
  }),

  layeredLayout: $(go.LayeredDigraphLayout, {
    direction: 90,
    columnSpacing: 8,
    layerSpacing: 12,
    aggressiveOption: go.LayeredDigraphLayout.AggressiveNone,
  }),
})

export const Diagram: FC = () => {
  const { layout, layeredIterations, layeredOptions } = useStore()

  const objectsRef = useRef<DiagramObjects>(null!)
  if (!objectsRef.current) objectsRef.current = makeDiagramObjects()

  useEffect(() => {
    if (!objectsRef.current) return
    const { diagram, treeLayout, layeredLayout } = objectsRef.current
    diagram.layout = layout === 'tree' ? treeLayout : layeredLayout
    layeredLayout.iterations = layeredIterations
    for (const key in layeredOptions)
      (layeredLayout as any)[key] = (go.LayeredDigraphLayout as any)[
        (layeredOptions as any)[key]
      ]

    diagram.layoutDiagram(true)
  }, [layout, layeredIterations, layeredOptions])

  return (
    <ReactDiagram
      {...useStore(selectDiagramData)}
      initDiagram={() => objectsRef.current?.diagram}
      skipsDiagramUpdate={false}
      divClassName="graphDiagram"
    />
  )
}
