import go from 'gojs'
import { color } from '../color'
import './shapes'
import { DiagramLinkData, DiagramModelData } from './types'

const $ = go.GraphObject.make

export const linkTemplateMap = new go.Map<
  DiagramLinkData['category'],
  go.Link
>()

type DiagramStemEdgeData = DiagramLinkData & { category: 'stemEdge' }

linkTemplateMap.add(
  'stemEdge',
  $(
    go.Link,
    {
      curve: go.Link.Bezier,
      fromEndSegmentLength: 24,
      toEndSegmentLength: 24,
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        const data = e.targetObject?.part?.data as DiagramStemEdgeData
        if (store.activation?.type === 'linking') store.cancelLinking()
        else if (data.stemIsActivated) store.activate(null)
        else
          store.activate({
            type: 'stem',
            from: data.from,
          })
      },
    },
    new go.Binding('layerName', '', (d: DiagramStemEdgeData) =>
      d.stemIsActivated || d.stemIsLinking ? 'activatedEdge' : ''
    ),
    $(
      go.Shape,
      { strokeWidth: 2 },
      new go.Binding('stroke', '', (d: DiagramStemEdgeData) =>
        d.stemIsActivated || d.stemIsLinking ? color.highlight : color.line
      ),
      new go.Binding('strokeWidth', 'isActivated', (v) => (v ? 4 : 2))
    )
  )
)

type DiagramGraphEdgeData = DiagramLinkData & { category: 'graphEdge' }

linkTemplateMap.add(
  'graphEdge',
  $(
    go.Link,
    {
      curve: go.Link.Bezier,
      fromEndSegmentLength: 24,
      toEndSegmentLength: 24,
      fromSpot: go.Spot.Bottom,
      toSpot: go.Spot.Top,
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        const data = e.targetObject?.part?.data as DiagramGraphEdgeData
        if (store.activation?.type === 'linking') store.cancelLinking()
        else if (data.isActivated) store.activate(null)
        else
          store.activate({
            type: 'edge',
            from: data.fromNode,
            fromBranch: data.fromBranch ?? undefined,
            to: data.to,
          })
      },
    },
    new go.Binding('isLayoutPositioned', 'isLayoutPositioned'),
    new go.Binding('layerName', '', (d: DiagramGraphEdgeData) =>
      d.isActivated ? 'activatedEdge' : ''
    ),
    $(
      go.Shape,
      { strokeWidth: 2 },
      new go.Binding('stroke', '', (d: DiagramGraphEdgeData) =>
        d.isActivated ? color.highlight : d.isOrphan ? color.error : color.line
      ),
      new go.Binding('strokeWidth', 'isActivated', (v) => (v ? 4 : 2)),
      new go.Binding('strokeDashArray', 'type', (v) =>
        v === 'dag' ? [5, 3] : v === 'cycle' ? [2, 2] : null
      )
    ),
    $(
      go.Panel,
      'Auto',
      {
        toolTip: $(
          go.Adornment,
          'Spot',
          { background: 'transparent' }, // avoid hiding tooltip when mouse moves
          $(go.Placeholder),
          $(
            go.Panel,
            'Auto',
            {
              alignment: go.Spot.Center,
              alignmentFocus: go.Spot.Center,
            },
            $(
              go.Shape,
              'pill',
              {
                fill: color.background,
              },
              new go.Binding('stroke', '', (d: DiagramGraphEdgeData) =>
                d.isActivated
                  ? color.highlight
                  : d.isOrphan
                  ? color.error
                  : color.line
              )
            ),
            $(
              go.TextBlock,
              {
                margin: new go.Margin(4, 8),
              },
              new go.Binding('text', 'label'),
              new go.Binding('stroke', '', (d: DiagramGraphEdgeData) =>
                d.isActivated
                  ? color.highlight
                  : d.isOrphan
                  ? color.error
                  : color.line
              )
            )
          )
        ),
      },
      new go.Binding('visible', 'label', (v) => v !== null),
      $(go.Shape, 'pill', {
        fill: color.background,
        stroke: null,
      }),
      $(
        go.TextBlock,
        {
          margin: new go.Margin(2, 4),
          maxSize: new go.Size(64, NaN),
          maxLines: 1,
          overflow: go.TextBlock.OverflowEllipsis,
        },
        new go.Binding('text', 'label'),
        new go.Binding('stroke', '', (d: DiagramGraphEdgeData) =>
          d.isActivated
            ? color.highlight
            : d.isOrphan
            ? color.error
            : color.line
        )
      )
    )
  )
)

type DiagramBranchEdgeData = DiagramLinkData & { category: 'branchEdge' }

linkTemplateMap.add(
  'branchEdge',
  $(
    go.Link,
    {
      curve: go.Link.Bezier,
      fromEndSegmentLength: 24,
      toEndSegmentLength: 24,
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        if (store.activation?.type === 'linking') store.cancelLinking()
        else store.activate(null)
      },
    },
    new go.Binding('layerName', '', (d: DiagramBranchEdgeData) =>
      d.branchIsActivated || d.branchIsLinking ? 'activatedEdge' : ''
    ),
    $(
      go.Shape,
      { strokeWidth: 2 },
      new go.Binding('stroke', '', (d: DiagramBranchEdgeData) =>
        d.branchIsActivated || d.branchIsLinking
          ? color.highlight
          : d.branchIsOrphan
          ? color.error
          : color.line
      )
    )
  )
)
