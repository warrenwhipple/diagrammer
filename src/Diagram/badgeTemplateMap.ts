import go from 'gojs'
import { color } from '../color'
import { iconGeometry, IconKey } from './iconGeometry'
import { DiagramBadgeData } from './types'

const diameter = 26

const $ = go.GraphObject.make

export const badgeTemplateMap = new go.Map<
  DiagramBadgeData['category'],
  go.Panel
>()

const makeTooltip = (background: string, text: string) =>
  $(
    go.Adornment,
    'Spot',
    { background: 'transparent' },
    $(go.Placeholder),
    $(
      go.Panel,
      'Auto',
      {
        alignment: go.Spot.Left,
        alignmentFocus: go.Spot.Right,
      },
      $(go.Shape, 'RoundedRectangle', {
        fill: background,
        stroke: null,
      }),
      $(go.TextBlock, {
        stroke: color.background,
        text,
        margin: 4,
      })
    )
  )

badgeTemplateMap.add(
  'error',
  $(
    go.Panel,
    'Spot',
    { toolTip: makeTooltip(color.error, 'Error') },
    $(go.Shape, 'Circle', {
      fill: color.background,
      stroke: null,
      width: diameter,
      height: diameter,
    }),
    $(
      go.Shape,
      {
        geometry: iconGeometry('error'),
        alignment: go.Spot.Center,
        fill: color.error,
        stroke: null,
        width: diameter,
        height: diameter,
      },
      new go.Binding()
    ),
    $(go.Shape, 'Circle', {
      alignment: go.Spot.Center,
      fill: null,
      stroke: color.background,
      strokeWidth: 2,
      width: diameter,
      height: diameter,
    })
  )
)

const makeBadge = (
  icon: IconKey,
  toolTipText: string,
  fill: string,
  scale: number = 1,
  angle: number = 0
) =>
  $(
    go.Panel,
    'Spot',
    { toolTip: toolTipText ? makeTooltip(fill, toolTipText) : undefined },
    $(go.Shape, 'Circle', {
      fill,
      stroke: color.background,
      strokeWidth: 2,
      width: diameter,
      height: diameter,
    }),
    $(go.Shape, {
      geometry: iconGeometry(icon),
      fill: color.background,
      stroke: null,
      scale,
      angle,
    })
  )

badgeTemplateMap.add(
  'upgrade',
  makeBadge('upgrade', 'Upgrade pending', color.info)
)
badgeTemplateMap.add('skip', makeBadge('skip', 'Skip', color.line, 0.875, 90))
badgeTemplateMap.add('block', makeBadge('backHand', 'Block', color.line, 0.75))
