import go from 'gojs'

go.Shape.defineFigureGenerator('Pill', (_, w, h) => {
  if (w > h) {
    const r = h / 2
    const wr = w - r
    return new go.Geometry().add(
      new go.PathFigure(r, h)
        .add(new go.PathSegment(go.PathSegment.Arc, 90, 180, r, r, r, r))
        .add(new go.PathSegment(go.PathSegment.Line, wr, 0))
        .add(
          new go.PathSegment(go.PathSegment.Arc, 270, 180, wr, r, r, r).close()
        )
    )
  } else {
    const r = w / 2
    const hr = h - r
    return new go.Geometry().add(
      new go.PathFigure(0, r)
        .add(new go.PathSegment(go.PathSegment.Arc, 180, 180, r, r, r, r))
        .add(new go.PathSegment(go.PathSegment.Line, w, hr))
        .add(
          new go.PathSegment(go.PathSegment.Arc, 0, 180, r, hr, r, r).close()
        )
    )
  }
})
