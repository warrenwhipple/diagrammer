import { AppStore } from '../store'
import { computeBranches } from '../utilities'
import { DiagramLinkData, DiagramNodeData } from './types'

export const selectDiagramData = (state: AppStore) => {
  const { nodesById, graph, rootId, ...modelData } = state
  const { activation, layout, branchRender } = modelData
  const isLabelBranches = branchRender === 'mid'

  const goNodesByKey: { [key: string]: DiagramNodeData } = {
    start: { key: 'start', index: 0, category: 'start' },
  }
  const goLinks: DiagramLinkData[] = []

  const actNodeId = activation?.type === 'node' && activation.nodeId
  const actBranchNodeId = activation?.type === 'branch' && activation.nodeId
  const actBranchBranch = activation?.type === 'branch' && activation.branch
  const actStemFrom = activation?.type === 'stem' && activation.from
  const actEdgeFrom = activation?.type === 'edge' && activation.from
  const actEdgeFromBranch = activation?.type === 'edge' && activation.fromBranch
  const actEdgeTo = activation?.type === 'edge' && activation.to
  const actLinkingFrom = activation?.type === 'linking' && activation.from
  const actLinkingFromBranch =
    activation?.type === 'linking' && activation.fromBranch

  const addBranchNode = (
    id: string,
    branch: string,
    index: number,
    isLinked: boolean,
    isOrphan: boolean
  ) => {
    const branchKey = `${id}:${branch}`
    const isActivated = actBranchNodeId === id && actBranchBranch === branch
    const isLinking = actLinkingFrom === id && actLinkingFromBranch === branch
    const nodeIsActivated = actNodeId === id
    const siblingIsActivated = actBranchNodeId === id
    goNodesByKey[branchKey] = {
      key: branchKey,
      category: 'branch',
      nodeId: id,
      text: branch,
      index,
      isLinked,
      isOrphan,
      isActivated,
      isLinking,
      nodeIsActivated,
      siblingIsActivated,
    }
    goLinks.push({
      key: `branchEdge:${branchKey}`,
      from: id,
      to: branchKey,
      category: 'branchEdge',
      toBranch: branch,
      branchIsOrphan: isOrphan,
      branchIsActivated: isActivated,
      branchIsLinking: isLinking,
      nodeIsActivated,
      siblingIsActivated,
    })
  }

  if (!graph.start?.length) {
    const stemKey = 'stem:start'
    const isActivated = actStemFrom === 'start'
    const isLinking =
      actLinkingFrom === 'start' && actLinkingFromBranch === undefined
    goNodesByKey[stemKey] = {
      key: stemKey,
      index: 0,
      category: 'stem',
      from: 'start',
      isActivated,
      isLinking,
    }
    goLinks.push({
      key: `stemEdge:${stemKey}`,
      from: 'start',
      to: stemKey,
      category: 'stemEdge',
      stemIsActivated: isActivated,
      stemIsLinking: isLinking,
    })
  }

  const branchesById: { [id: string]: string[] | undefined } = {}

  for (const id in nodesById) {
    const node = nodesById[id]
    const computedBranches = computeBranches(node, true)
    if (computedBranches) branchesById[id] = computedBranches
    const { name, isError, isUpgrade, isSkip, isBlock } = node
    goNodesByKey[id] = {
      key: id,
      category: 'node',
      text: name,
      index: 0,
      isActivated: id === actNodeId,
      isError: !!isError,
      isUpgrade: !!isUpgrade,
      isSkip: !!isSkip,
      isBlock: !!isBlock,
    }
    if (computedBranches?.length) {
      computedBranches.forEach((branch, index) => {
        const isLinked = !!graph[id].find((e) => e.fromBranch === branch)
        if (!isLabelBranches || !isLinked)
          addBranchNode(id, branch, index, isLinked, false)
      })
    } else if (!graph[id].length) {
      const stemKey = `stem:${id}`
      const isActivated = actStemFrom === id
      const isLinking =
        actLinkingFrom === id && actLinkingFromBranch === undefined
      goNodesByKey[stemKey] = {
        key: stemKey,
        index: 0,
        category: 'stem',
        from: id,
        isActivated,
        isLinking,
      }
      goLinks.push({
        key: `stemEdge:${stemKey}`,
        from: id,
        to: stemKey,
        category: 'stemEdge',
        stemIsActivated: isActivated,
        stemIsLinking: isLinking,
      })
    }
  }

  for (const from in graph) {
    const edges = graph[from]
    let orphanIndex = branchesById[from]?.length || 0
    for (const { from, fromBranch, to, type } of edges) {
      const branchIndex =
        fromBranch === undefined
          ? -1
          : branchesById[from]?.indexOf(fromBranch) ?? -1
      const isOrphan = fromBranch !== undefined && branchIndex === -1
      if (isOrphan) {
        if (isLabelBranches) goNodesByKey[to].index = orphanIndex++
        else addBranchNode(from, fromBranch, orphanIndex++, false, true)
      } else if (isLabelBranches) {
        goNodesByKey[to].index = branchIndex
      }
      goLinks.push({
        key: `edge:${type}:${from}:${fromBranch}:${to}`,
        from:
          fromBranch === undefined
            ? from
            : isLabelBranches
            ? from
            : `${from}:${fromBranch}`,
        to,
        category: 'graphEdge',
        type,
        fromNode: from,
        label: isLabelBranches && fromBranch !== undefined ? fromBranch : null,
        fromBranch: fromBranch ?? null,
        isOrphan,
        isActivated:
          actEdgeFrom === from &&
          actEdgeFromBranch === fromBranch &&
          actEdgeTo === to,
        isLayoutPositioned: layout === 'layered' || type === 'tree',
      })
    }
  }

  return {
    nodeDataArray: Object.values(goNodesByKey),
    linkDataArray: goLinks,
    modelData,
  }
}
