import { AppStore } from '../store'

export type DiagramNodeData = { key: string; index: number } & (
  | {
      category: 'start'
    }
  | {
      category: 'node'
      text: string
      isActivated: boolean
      isError: boolean
      isUpgrade: boolean
      isSkip: boolean
      isBlock: boolean
    }
  | {
      category: 'branch'
      nodeId: string
      text: string
      isLinked: boolean
      isOrphan: boolean
      isActivated: boolean
      isLinking: boolean
      nodeIsActivated: boolean
      siblingIsActivated: boolean
    }
  | {
      category: 'stem'
      from: string
      isActivated: boolean
      isLinking: boolean
    }
)

export type DiagramLinkData = {
  key: string
  from: string
  to: string
} & (
  | {
      category: 'graphEdge'
      type: 'tree' | 'dag' | 'cycle'
      fromNode: string
      label: string | null
      fromBranch: string | null
      isOrphan: boolean
      isActivated: boolean
      isLayoutPositioned: boolean
    }
  | {
      category: 'branchEdge'
      toBranch: string
      branchIsOrphan: boolean
      branchIsActivated: boolean
      branchIsLinking: boolean
      nodeIsActivated: boolean
      siblingIsActivated: boolean
    }
  | {
      category: 'stemEdge'
      stemIsActivated: boolean
      stemIsLinking: boolean
    }
)

export type DiagramBadgeData = {
  category: 'error' | 'upgrade' | 'skip' | 'block'
}

export type DiagramModelData = Omit<AppStore, 'nodesById' | 'edges' | 'rootId'>

export type DiagramPartData = DiagramNodeData | DiagramLinkData
