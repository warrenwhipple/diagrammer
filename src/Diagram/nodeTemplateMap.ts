import go from 'gojs'
import { color } from '../color'
import { badgeTemplateMap } from './badgeTemplateMap'
import './shapes'
import { DiagramBadgeData, DiagramModelData, DiagramNodeData } from './types'

const gridWidth = 128
const gridHeight = 80

const $ = go.GraphObject.make

export const nodeTemplateMap = new go.Map<
  DiagramNodeData['category'],
  go.Node
>()

nodeTemplateMap.add(
  'start',
  $(
    go.Node,
    'Auto',
    {
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        store.activate(null)
      },
    },
    $(go.Shape, 'Circle', {
      fill: color.background,
      stroke: color.line,
      strokeWidth: 2,
    }),
    $(go.TextBlock, { text: 'Start', stroke: color.line })
  )
)

const badgeFn = (node: DiagramNodeData): DiagramBadgeData[] => {
  const badges: DiagramBadgeData[] = []
  if (node.category !== 'node') return badges
  if (node.isError) badges.push({ category: 'error' })
  if (node.isUpgrade) badges.push({ category: 'upgrade' })
  if (node.isSkip) badges.push({ category: 'skip' })
  if (node.isBlock) badges.push({ category: 'block' })
  return badges
}

type DiagramNodeNodeData = DiagramNodeData & { category: 'node' }

nodeTemplateMap.add(
  'node',
  $(
    go.Node,
    'Spot',
    {
      locationObjectName: 'body',
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        const data = e.targetObject?.part?.data as DiagramNodeNodeData
        if (store.activation?.type === 'linking')
          store.completeLinking({ to: data.key })
        else if (data.isActivated) store.activate(null)
        else store.activate({ type: 'node', nodeId: data.key })
      },
    },
    $(
      go.Panel,
      'Auto',
      { name: 'body', portId: '' },
      $(
        go.Shape,
        'RoundedRectangle',
        {
          width: gridWidth,
          height: gridHeight,
          fill: color.node,
          strokeWidth: 4,
        },
        new go.Binding('stroke', 'isActivated', (v) =>
          v ? color.highlight : null
        )
      ),
      $(go.TextBlock, new go.Binding('text', 'text'), {
        stroke: color.background,
      })
    ),
    $(
      go.Panel,
      'Vertical',
      {
        alignment: go.Spot.LeftCenter,
        itemTemplateMap: badgeTemplateMap,
      },
      new go.Binding('itemArray', '', badgeFn)
    )
  )
)

type DiagramBranchData = DiagramNodeData & { category: 'branch' }

nodeTemplateMap.add(
  'branch',
  $(
    go.Node,
    'Auto',
    {
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        const data = e.targetObject?.part?.data as DiagramBranchData
        if (store.activation?.type === 'linking') store.cancelLinking()
        else if (data.isActivated) store.activate(null)
        else
          store.activate({
            type: 'branch',
            nodeId: data.nodeId,
            branch: data.text,
          })
      },
    },
    $(
      go.Shape,
      'Pill',
      {
        portId: '',
        stroke: null,
        minSize: new go.Size(22, 22),
      },
      new go.Binding('fill', '', (d: DiagramBranchData) =>
        d.isLinked
          ? color.background
          : d.isActivated || d.isLinking
          ? color.highlight
          : d.isOrphan
          ? color.error
          : color.line
      )
    ),
    $(
      go.TextBlock,
      {
        margin: new go.Margin(4, 6),
      },
      new go.Binding('text', 'text'),
      new go.Binding('stroke', '', (d: DiagramBranchData) =>
        d.isLinked
          ? d.isActivated || d.isLinking
            ? color.highlight
            : d.isOrphan
            ? color.error
            : color.line
          : color.background
      )
    )
  )
)

type DiagramStemData = DiagramNodeData & { category: 'stem' }

nodeTemplateMap.add(
  'stem',
  $(
    go.Node,
    'Auto',
    {
      click: (e) => {
        const store = e.diagram.model.modelData as DiagramModelData
        const data = e.targetObject?.part?.data as DiagramStemData
        if (store.activation?.type === 'linking') store.cancelLinking()
        else if (data.isActivated) store.activate(null)
        else store.activate({ type: 'stem', from: data.from })
      },
    },
    $(
      go.Shape,
      'Circle',
      {
        portId: '',
        stroke: null,
        minSize: new go.Size(22, 22),
      },
      new go.Binding('fill', '', (v: DiagramStemData) =>
        v.isActivated || v.isLinking ? color.highlight : color.line
      )
    ),
    $(
      go.TextBlock,
      { stroke: color.background },
      new go.Binding('text', 'isLinking', (v) => (v ? '↓' : '+'))
    )
  )
)
