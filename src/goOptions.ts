export const layeredOptionsData = {
  aggressiveOption: {
    label: 'Cross reduction',
    options: [
      { label: 'none', value: 'AggressiveNone' },
      { label: 'less', value: 'AggressiveLess' },
      { label: 'more', value: 'AggressiveMore' },
    ],
  },
  cycleRemoveOption: {
    label: 'Cycle detection',
    options: [
      { label: 'depth first', value: 'CycleDepthFirst' },
      // { label: 'from layers', value: 'CycleFromLayers' },
      { label: 'greedy', value: 'CycleGreedy' },
    ],
  },
  initializeOption: {
    label: 'Init',
    options: [
      { label: 'depth first in', value: 'InitDepthFirstIn' },
      { label: 'depth first out', value: 'InitDepthFirstOut' },
      { label: 'naive', value: 'InitNaive' },
    ],
  },
  layeringOption: {
    label: 'Layering',
    options: [
      { label: 'sink', value: 'LayerLongestPathSink' },
      { label: 'source', value: 'LayerLongestPathSource' },
      { label: 'optimal', value: 'LayerOptimalLinkLength' },
    ],
  },
  packOption: {
    label: 'Pack',
    options: [
      { label: 'all', value: 'PackAll' },
      { label: 'expand', value: 'PackExpand' },
      { label: 'median', value: 'PackMedian' },
      { label: 'none', value: 'PackNone' },
      { label: 'straighten', value: 'PackStraighten' },
    ],
  },
}

export type LayeredOptionKey = keyof typeof layeredOptionsData

export type LayeredOptions = Record<LayeredOptionKey, string>

export const initialLayeredOptions: LayeredOptions = {
  aggressiveOption: 'AggressiveNone',
  cycleRemoveOption: 'CycleDepthFirst',
  initializeOption: 'InitDepthFirstOut',
  layeringOption: 'LayerLongestPathSource',
  packOption: 'PackStraighten',
}
