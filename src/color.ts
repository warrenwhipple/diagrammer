export const color = {
  background: '#fff',
  error: '#d50000',
  errorLabel: '#d50000dd',
  highlight: '#0091ea',
  highlightLabel: '#0091eadd',
  info: '#0064b7',
  label: '#dddd',
  line: '#444',
  lineDim: '#888',
  node: '#004e6e',
}
