import { FC } from 'react'
import { useStore } from '../store'

export interface EdgeDetailsProps {
  from: string
  fromBranch?: string
  to: string
}

export const EdgeDetails: FC<EdgeDetailsProps> = ({ from, fromBranch, to }) => {
  const { graph, deleteEdge, promoteEdge } = useStore()
  const edge = graph[from].find(
    (e) => e.fromBranch === fromBranch && e.to === to
  )
  if (!edge) return null
  const { type } = edge
  return (
    <>
      <p className="superTitle">{type} Edge</p>
      {fromBranch === undefined ? null : (
        <p>
          Branch: <b>{fromBranch}</b>
        </p>
      )}
      <p>
        <button onClick={() => deleteEdge({ from, fromBranch, to })}>
          Delete edge
        </button>
      </p>
      {type === 'dag' && (
        <p>
          <button onClick={() => promoteEdge({ from, fromBranch, to })}>
            Promote edge
          </button>
        </p>
      )}
    </>
  )
}
