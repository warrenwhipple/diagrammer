import { FC } from 'react'
import { AppendNodeControl } from './AppendNodeControl'
import { LinkingButton } from './LinkingButton'

export interface StemDetailsProps {
  from: string
}

export const StemDetails: FC<StemDetailsProps> = ({ from }) => (
  <>
    <p className="superTitle">Next</p>
    <p>
      <AppendNodeControl nodeId={from} />
    </p>
    <p>
      <LinkingButton />
    </p>
  </>
)
