import { FC } from 'react'
import { color } from '../color'
import { useStore } from '../store'

export const LinkingButton: FC = () => {
  const { activation, activate, cancelLinking } = useStore()
  if (activation?.type === 'linking')
    return (
      <button
        style={{
          color: color.background,
          backgroundColor: color.highlight,
        }}
        onClick={cancelLinking}
      >
        Linking
      </button>
    )
  else if (activation?.type === 'branch')
    return (
      <button
        onClick={() =>
          activate({
            type: 'linking',
            from: activation.nodeId,
            fromBranch: activation.branch,
          })
        }
      >
        Link
      </button>
    )
  else if (activation?.type === 'stem')
    return (
      <button
        onClick={() =>
          activate({
            type: 'linking',
            from: activation.from,
          })
        }
      >
        Link
      </button>
    )
  return null
}
