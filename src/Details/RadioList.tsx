import { Dispatch, FC } from 'react'

export interface RadioListProps {
  name: string
  choices: string[] | { label: string; value: string }[]
  selectedChoice: string
  onChange: Dispatch<string>
}

export const RadioList: FC<RadioListProps> = ({
  name,
  choices,
  selectedChoice,
  onChange,
}) => {
  return (
    <>
      {choices.map((choice) => {
        const label = typeof choice === 'string' ? choice : choice.label
        const value = typeof choice === 'string' ? choice : choice.value
        return (
          <label key={value} className="radioChoice">
            <input
              type="radio"
              name={name}
              value={value}
              checked={value === selectedChoice}
              onChange={(e) => {
                if (e.target.checked) onChange(value)
              }}
            />
            {label}
          </label>
        )
      })}
    </>
  )
}
