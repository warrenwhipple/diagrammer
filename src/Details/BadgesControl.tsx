import React, { FC } from 'react'
import { useStore } from '../store'

const flags = ['isError', 'isUpgrade', 'isSkip', 'isBlock'] as const

export interface BadgesControlProps {
  nodeId: string
}

export const BadgesControl: FC<BadgesControlProps> = ({ nodeId }) => {
  const { nodesById, toggleNodeFlag } = useStore()
  const node = nodesById[nodeId]
  return (
    <div className="para">
      Badges:
      {flags.map((flag) => (
        <label key={flag} className="radioChoice">
          <input
            type="checkbox"
            name={flag}
            checked={!!node[flag]}
            onChange={() => {
              toggleNodeFlag({ nodeId, flag })
            }}
          />
          {flag.slice(2).toLowerCase()}
        </label>
      ))}
    </div>
  )
}
