import { FC, useMemo } from 'react'
import { useStore } from '../store'
import { BranchDetails } from './BranchDetails'
import { EdgeDetails } from './EdgeDetails'
import { NodeDetails } from './NodeDetails'
import { StemDetails } from './StemDetails'

export const ActiveDetails: FC = () => {
  const { activation, nodesById } = useStore()

  const isValid = useMemo(() => {
    if (activation === null) return true
    switch (activation.type) {
      case 'node':
      case 'branch':
        return activation.nodeId in nodesById
      case 'stem':
      case 'linking':
        return activation.from in nodesById || activation.from === 'start'
      case 'edge':
        return activation.from in nodesById && activation.to in nodesById
    }
  }, [activation, nodesById])

  if (!isValid || !activation) return null

  switch (activation.type) {
    case 'node':
      return <NodeDetails nodeId={activation.nodeId} />
    case 'branch':
      return (
        <BranchDetails nodeId={activation.nodeId} branch={activation.branch} />
      )
    case 'stem':
      return <StemDetails from={activation.from} />
    case 'edge':
      return (
        <EdgeDetails
          from={activation.from}
          fromBranch={activation.fromBranch}
          to={activation.to}
        />
      )
    case 'linking':
      return activation.fromBranch === undefined ? (
        <StemDetails from={activation.from} />
      ) : (
        <BranchDetails
          nodeId={activation.from}
          branch={activation.fromBranch}
        />
      )
  }
}
