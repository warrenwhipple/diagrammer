import { FC, Fragment } from 'react'
import { useStore } from '../store'

const nums = [2, 3, 4, 5, 6, 7, 8]

export interface AppendNodeControlProps {
  nodeId: string
  fromBranch?: string
}

export const AppendNodeControl: FC<AppendNodeControlProps> = ({
  nodeId,
  fromBranch,
}) => {
  const { appendNode } = useStore()

  return (
    <>
      <button onClick={() => appendNode({ from: nodeId, fromBranch })}>
        Add node
      </button>{' '}
      with branches{' '}
      {nums.map((branchCount) => (
        <Fragment key={branchCount}>
          <button
            onClick={() => {
              appendNode({ from: nodeId, fromBranch, branchCount })
            }}
          >
            {branchCount}
          </button>{' '}
        </Fragment>
      ))}
    </>
  )
}
