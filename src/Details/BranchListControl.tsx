import { FC } from 'react'
import { useStore } from '../store'
import { greekAlphabet, spliced } from '../utilities'

export interface BranchListControlProps {
  nodeId: string
}

export const BranchListControl: FC<BranchListControlProps> = ({ nodeId }) => {
  const { nodesById, setBranches, setFallbackBranch } = useStore()
  const node = nodesById[nodeId]
  const branches = node.branches || []
  return (
    <>
      <div className="branchTable">
        {branches.map((branch, index) => (
          <div key={index} className="branchTableRow">
            <input
              value={branch}
              onChange={(e) => {
                setBranches({
                  nodeId,
                  action: (prev) => spliced(prev, index, 1, e.target.value),
                })
              }}
              className="inputTableCell"
            />
            <div className="buttonTableCell">
              <button
                onClick={() => {
                  setBranches({
                    nodeId,
                    action: (prev) => {
                      const next = [...prev]
                      next.splice(index, 1)
                      return next
                    },
                  })
                }}
              >
                ×
              </button>
            </div>
          </div>
        ))}
        <div className="branchTableRow">
          <input
            value={node.fallbackBranch || ''}
            onChange={(e) => {
              setFallbackBranch({ nodeId, branch: e.target.value })
            }}
            className="inputTableCell"
          />
          <div className="infoTableCell">default fallback</div>
        </div>
      </div>

      <button
        onClick={() => {
          setBranches({
            nodeId,
            action: (prev) => {
              const s = new Set(greekAlphabet)
              prev.forEach((branch) => {
                s.delete(branch)
              })
              const name: string =
                s.values().next().value || `branch${prev.length}`
              return [...prev, name]
            },
          })
        }}
      >
        +
      </button>
    </>
  )
}
