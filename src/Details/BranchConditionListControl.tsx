import { highlight, languages } from 'prismjs'
import 'prismjs/themes/prism.css'
import { CSSProperties, FC } from 'react'
import Editor from 'react-simple-code-editor'
import { useStore } from '../store'
import { greekAlphabet, spliced } from '../utilities'

const highlightJs = (value: string) =>
  highlight(value, languages.js, 'javascript')

const editorStyle: CSSProperties = { overflow: 'inherit' }

export interface BranchConditionListControlProps {
  nodeId: string
}

export const BranchConditionListControl: FC<BranchConditionListControlProps> =
  ({ nodeId }) => {
    const { nodesById, setBranchConditions, setFallbackBranch } = useStore()
    const node = nodesById[nodeId]
    const branches = node.branchConditions || []
    return (
      <>
        <div className="branchConditionTable">
          {branches.map(({ name, condition }, index) => (
            <div key={index} className="branchConditionTableRow">
              <input
                value={name}
                onChange={(e) => {
                  setBranchConditions({
                    nodeId,
                    action: (prev) =>
                      spliced(prev, index, 1, {
                        ...prev[index],
                        name: e.target.value,
                      }),
                  })
                }}
                className="inputTableCell"
              />
              <Editor
                value={condition}
                onValueChange={(condition) => {
                  setBranchConditions({
                    nodeId,
                    action: (prev) =>
                      spliced(prev, index, 1, {
                        ...prev[index],
                        condition,
                      }),
                  })
                }}
                highlight={highlightJs}
                padding={8}
                style={editorStyle}
                className="scriptTableCell"
              />
              <div className="buttonTableCell">
                <button
                  onClick={() => {
                    setBranchConditions({
                      nodeId,
                      action: (prev) => {
                        const next = [...prev]
                        next.splice(index, 1)
                        return next
                      },
                    })
                  }}
                >
                  ×
                </button>
              </div>
            </div>
          ))}
          <div className="branchConditionTableRow">
            <input
              value={node.fallbackBranch || ''}
              onChange={(e) => {
                setFallbackBranch({ nodeId, branch: e.target.value })
              }}
              className="inputTableCell"
            />
            <div className="infoTableCell">default fallback</div>
            <div className="buttonTableCell">
              <button disabled style={{ visibility: 'hidden' }}>
                ×
              </button>
            </div>
          </div>
        </div>

        <button
          onClick={() => {
            setBranchConditions({
              nodeId,
              action: (prev) => {
                const s = new Set(greekAlphabet)
                prev.forEach(({ name }) => {
                  s.delete(name)
                })
                const name: string =
                  s.values().next().value || `branch${prev.length}`
                return [
                  ...prev,
                  { name, condition: `data.choice === '${name}'` },
                ]
              },
            })
          }}
        >
          +
        </button>
      </>
    )
  }
