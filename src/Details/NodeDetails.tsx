import { FC } from 'react'
import { GraphNode, useStore } from '../store'
import { BranchConditionListControl } from './BranchConditionListControl'
import { BranchDecisionScriptControl } from './BranchDecisionScriptControl'
import { BranchesScriptControl } from './BranchesScriptControl'
import { BranchListControl } from './BranchListControl'
import { RadioList } from './RadioList'

type BranchMode = GraphNode['branchMode']

const branchModes: BranchMode[] = [
  'none',
  'list',
  'conditionList',
  'listScript',
]

export interface NodeDetailsProps {
  nodeId: string
}

export const NodeDetails: FC<NodeDetailsProps> = ({ nodeId }) => {
  const { nodesById, deleteNode, setBranchMode, setFallbackBranch } = useStore()
  const node = nodesById[nodeId]
  const { name, branchMode } = node
  return (
    <>
      <p className="superTitle">Node</p>
      <h2>{name}</h2>
      <p>
        <button
          onClick={() => {
            deleteNode({ nodeId })
          }}
        >
          Delete node
        </button>
      </p>
      <div className="para">
        Branch mode:
        <RadioList
          name="branchMode"
          choices={branchModes}
          selectedChoice={branchMode}
          onChange={(v) => {
            setBranchMode({ nodeId, mode: v as BranchMode })
          }}
        />
      </div>
      {branchMode === 'list' && (
        <>
          <p className="inputLabel">Branches</p>
          <BranchListControl nodeId={nodeId} />
        </>
      )}
      {branchMode === 'conditionList' && (
        <>
          <p className="inputLabel">Branch conditions</p>
          <BranchConditionListControl nodeId={nodeId} />
        </>
      )}
      {branchMode === 'listScript' && (
        <>
          <p className="inputLabel">Branch list script</p>
          <BranchesScriptControl nodeId={nodeId} />
          <p className="inputLabel">Default fallback branch</p>
          <div className="branchTable">
            <div className="branchTableRow">
              <input
                value={node.fallbackBranch || ''}
                onChange={(e) => {
                  setFallbackBranch({ nodeId, branch: e.target.value })
                }}
                className="inputTableCell"
              />
              <div className="infoTableCell">default fallback</div>
            </div>
          </div>
        </>
      )}
      {(branchMode === 'list' || branchMode === 'listScript') && (
        <>
          <p className="inputLabel">Branch decision script</p>
          <BranchDecisionScriptControl nodeId={nodeId} />
        </>
      )}
    </>
  )
}
