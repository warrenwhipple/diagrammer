import { highlight, languages } from 'prismjs'
import 'prismjs/themes/prism.css'
import { FC } from 'react'
import Editor from 'react-simple-code-editor'
import { useStore } from '../store'

const highlightFn = (value: string) =>
  highlight(value, languages.js, 'javascript')

export interface BranchDecisionScriptControlProps {
  nodeId: string
}

export const BranchDecisionScriptControl: FC<BranchDecisionScriptControlProps> =
  ({ nodeId }) => {
    const { nodesById, setBranchDecisionScript } = useStore()
    const script = nodesById[nodeId].branchDecisionScript

    return (
      <Editor
        value={script || ''}
        onValueChange={(value) => {
          setBranchDecisionScript({ nodeId, script: value })
        }}
        highlight={highlightFn}
        padding={8}
        className="scriptEditor"
        textareaClassName="scriptTextarea"
      />
    )
  }
