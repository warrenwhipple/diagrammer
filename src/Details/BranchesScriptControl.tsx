import { highlight, languages } from 'prismjs'
import 'prismjs/themes/prism.css'
import { FC, useEffect, useState } from 'react'
import Editor from 'react-simple-code-editor'
import { useStore } from '../store'

const highlightFn = (value: string) =>
  highlight(value, languages.js, 'javascript')

export interface BranchesScriptControlProps {
  nodeId: string
}

export const BranchesScriptControl: FC<BranchesScriptControlProps> = ({
  nodeId,
}) => {
  const { nodesById, setBranchesScript } = useStore()
  const script = nodesById[nodeId].branchesScript
  const [value, setValue] = useState(script || '')

  useEffect(() => {
    setValue(script || '')
  }, [nodeId, script])

  return (
    <>
      <Editor
        value={value}
        onValueChange={setValue}
        highlight={highlightFn}
        padding={8}
        className="scriptEditor"
        textareaClassName="scriptTextarea"
      />
      <button
        onClick={() => {
          setBranchesScript({ nodeId, script: value })
        }}
        disabled={value === script}
      >
        Save script
      </button>
    </>
  )
}
