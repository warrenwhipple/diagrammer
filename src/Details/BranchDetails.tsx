import { FC } from 'react'
import { AppendNodeControl } from './AppendNodeControl'
import { LinkingButton } from './LinkingButton'

export type BranchDetailsProps = {
  nodeId: string
  branch: string
}

export const BranchDetails: FC<BranchDetailsProps> = ({ nodeId, branch }) => (
  <>
    <p className="superTitle">Branch</p>
    <h2>{branch}</h2>
    <p>
      <AppendNodeControl nodeId={nodeId} fromBranch={branch} />
    </p>
    <p>
      <LinkingButton />
    </p>
  </>
)
