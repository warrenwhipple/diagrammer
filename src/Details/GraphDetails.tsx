import { FC } from 'react'
import { LayeredOptionKey, layeredOptionsData } from '../goOptions'
import { AppState, useStore } from '../store'
import { RadioList } from './RadioList'

const branchRenders: AppState['branchRender'][] = ['mid', 'layout']
const layouts: AppState['layout'][] = ['tree', 'layered']
const optionKeys = Object.keys(layeredOptionsData) as LayeredOptionKey[]

export const GraphDetails: FC = () => {
  const {
    history,
    future,
    branchRender,
    layout,
    layeredIterations,
    layeredOptions,
    undo,
    redo,
    reset,
    setBranchRender,
    setLayout,
    setLayeredIterations,
    setLayeredOption,
  } = useStore()
  return (
    <>
      <p className="superTitle">Graph</p>
      <p>
        <button onClick={undo} disabled={!history.length}>
          Undo ({history.length || 0})
        </button>{' '}
        <button onClick={redo} disabled={!future.length}>
          Redo ({future.length || 0})
        </button>{' '}
        <button onClick={reset}>Reset</button>
      </p>
      <p>
        Branch label positioning:
        <RadioList
          name="branchRender"
          choices={branchRenders}
          selectedChoice={branchRender}
          onChange={(v) => setBranchRender(v as AppState['branchRender'])}
        />
      </p>
      <div className="para">
        Layout:
        <RadioList
          name="layout"
          choices={layouts}
          selectedChoice={layout}
          onChange={(v) => setLayout(v as AppState['layout'])}
        />
        {layout === 'layered' && (
          <div className="indent">
            <div>
              Iterations:{' '}
              <input
                type="number"
                value={layeredIterations}
                min={0}
                onChange={(e) => {
                  setLayeredIterations(e.target.valueAsNumber)
                }}
                className="numberInput"
              />
            </div>
            {optionKeys.map((key) => {
              const { label, options } = layeredOptionsData[key]
              return (
                <div key={key}>
                  {label}:
                  <RadioList
                    name={key}
                    choices={options}
                    selectedChoice={layeredOptions[key]}
                    onChange={(value) => {
                      setLayeredOption({ key, value })
                    }}
                  />
                </div>
              )
            })}
          </div>
        )}
      </div>
    </>
  )
}
