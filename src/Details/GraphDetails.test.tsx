import { render, screen } from '@testing-library/react'
import { GraphDetails } from './GraphDetails'

test('renders graph details title', () => {
  render(<GraphDetails />)
  const button = screen.getByText(/graph/i)
  expect(button).toBeInTheDocument()
})
