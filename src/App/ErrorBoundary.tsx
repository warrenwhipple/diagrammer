import { Component, ErrorInfo, ReactNode } from 'react'
import { useStore } from '../store'

interface Props {
  children: ReactNode
}

interface State {
  hasError: boolean
  error?: Error
}

export class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError(error: Error): State {
    return {
      hasError: true,
      error: process.env.NODE_ENV === 'development' ? error : undefined,
    }
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error('Uncaught error:', error, errorInfo)
  }

  public render() {
    if (this.state.hasError) {
      return (
        <div style={{ color: 'darkred' }}>
          <h2>{this.state.error?.name || 'Error'}</h2>
          <p>{this.state.error?.message || 'Something went wrong'}</p>
          <p>
            <ResetButton />
          </p>
        </div>
      )
    }

    return this.props.children
  }
}

const ResetButton = () => (
  <button onClick={useStore((s) => s.reset)}>Reset Graph</button>
)
