import { ActiveDetails, GraphDetails } from '../Details'
import { Diagram } from '../Diagram'
import { ErrorBoundary } from './ErrorBoundary'

export const App = () => {
  return (
    <div className="app">
      <ErrorBoundary>
        <Diagram />
      </ErrorBoundary>
      <div className="activeDetails">
        <ErrorBoundary>
          <ActiveDetails />
        </ErrorBoundary>
      </div>
      <div className="graphDetails">
        <ErrorBoundary>
          <GraphDetails />
        </ErrorBoundary>
      </div>
    </div>
  )
}
