import { v4 } from 'uuid'
import { GraphNode } from './store'

/** fp equivalent of `Array.splice()` O(n) */
export const spliced = <T>(
  array: T[],
  start: number,
  deleteCount?: number,
  ...items: T[]
): T[] => {
  const newArray = [...array]
  newArray.splice(start, deleteCount || 0, ...items)
  return newArray
}

export const genId = () => v4()

export const greekAlphabet = [
  'alpha',
  'beta',
  'gamma',
  'delta',
  'epsilon',
  'zeta',
  'eta',
  'theta',
  'iota',
  'kappa',
  'lambda',
  'mu',
  'nu',
  'xi',
  'omicron',
  'pi',
  'rho',
  'sigma',
  'tau',
  'upsilon',
  'phi',
  'chi',
  'psi',
  'omega',
]

export const computeBranches = (
  node: GraphNode,
  includeFallback: boolean
): string[] | undefined => {
  let computedBranches: string[] | undefined
  switch (node.branchMode) {
    case 'none':
      return
    case 'list':
      computedBranches = node.branches && [...node.branches]
      break
    case 'listScript':
      if (node.branchesScript) {
        try {
          const result = eval(node.branchesScript) // eslint-disable-line no-eval
          if (!Array.isArray(result)) throw Error('Not an array')
          if (result.findIndex((v) => typeof v !== 'string') >= 0)
            throw Error('Array contains non strings')
          computedBranches = result
        } catch {}
      }
      break
    case 'conditionList':
      computedBranches = node.branchConditions?.map((v) => v.name)
  }
  if (includeFallback && node.fallbackBranch) {
    if (computedBranches) computedBranches.push(node.fallbackBranch)
    else return [node.fallbackBranch]
  }
  return computedBranches && [...new Set(computedBranches)]
}
