import produce from 'immer'
import { Dispatch, SetStateAction } from 'react'
import create, { SetState, State, StateCreator } from 'zustand'
import { persist } from 'zustand/middleware'
import { initialLayeredOptions, LayeredOptionKey } from './goOptions'
import { computeBranches, genId, greekAlphabet } from './utilities'

export interface BranchCondition {
  name: string
  condition: string
}

export interface GraphNode {
  id: string
  name: string
  branchMode: 'none' | 'list' | 'conditionList' | 'listScript'
  branchDecisionScript?: string
  branches?: string[]
  branchConditions?: BranchCondition[]
  branchesScript?: string
  fallbackBranch?: string
  isError?: boolean
  isUpgrade?: boolean
  isSkip?: boolean
  isBlock?: boolean
}

export interface GraphEdge {
  from: string
  fromBranch?: string
  to: string
  type: 'tree' | 'dag' | 'cycle'
}

export interface Graph {
  [from: string]: GraphEdge[]
}

export type GraphActivation =
  | { type: 'node'; nodeId: string }
  | { type: 'branch'; nodeId: string; branch: string }
  | { type: 'stem'; from: string }
  | { type: 'edge'; from: string; fromBranch?: string; to: string }
  | { type: 'linking'; from: string; fromBranch?: string }

/**
 * Depth first search acyclic edges only.
 * Pure fn. O(n)
 * */
const dagDfs = (
  graph: Graph,
  startNodeId: string,
  findNodeId: string
): boolean => {
  const visited = new Set<string>()
  const search = (v: string): boolean => {
    if (visited.has(v)) return false
    if (v === findNodeId) return true
    visited.add(v)
    for (const w of graph[v])
      if (w.type !== 'cycle' && search(w.to)) return true
    return false
  }
  return search(startNodeId)
}

/**
 * Promote all cycle edges to dag edges where possible.
 * Mutator. O(n^2)
 * */
const promoteCycles = (graph: Graph) => {
  for (const id in graph)
    for (const edge of graph[id])
      if (edge.type === 'cycle' && !dagDfs(graph, edge.to, edge.from))
        edge.type = 'dag'
}

/** Pure fn. O(n) */
const findEdge = (graph: Graph, predicate: (e: GraphEdge) => boolean) => {
  for (const from in graph)
    for (const edge of graph[from]) if (predicate(edge)) return edge
}

/** Pure fn. O(n) */
const findEdges = (graph: Graph, predicate: (e: GraphEdge) => boolean) => {
  const edges: GraphEdge[] = []
  for (const from in graph)
    for (const edge of graph[from]) if (predicate(edge)) edges.push(edge)
  return edges
}

export interface AppSnapshot {
  nodesById: { [id: string]: GraphNode }
  graph: Graph
  rootId: string | null
}

export interface AppState {
  nodesById: { [id: string]: GraphNode }
  graph: Graph
  rootId: string | null
  history: AppSnapshot[]
  future: AppSnapshot[]
  activation: GraphActivation | null
  branchRender: 'mid' | 'layout'
  layout: 'tree' | 'layered'
  layeredIterations: number
  layeredOptions: Record<LayeredOptionKey, string>
}

const initialState: AppState = {
  nodesById: {},
  graph: { start: [] },
  rootId: null,
  history: [],
  future: [],
  activation: null,
  branchRender: 'mid',
  layout: 'layered',
  layeredIterations: 4,
  layeredOptions: initialLayeredOptions,
}

const defaultBranchDecisionScript = 'data.branch'
const defaultFallbackBranch = 'else'

export interface AppActions {
  undo: () => void
  redo: () => void
  reset: () => void
  setBranchRender: Dispatch<AppState['branchRender']>
  setLayout: Dispatch<AppState['layout']>
  setLayeredIterations: Dispatch<number>
  setLayeredOption: Dispatch<{ key: LayeredOptionKey; value: string }>
  activate: Dispatch<GraphActivation | null>
  appendNode: Dispatch<{
    from: string
    fromBranch?: string
    branchCount?: number
  }>
  completeLinking: Dispatch<{ to: string }>
  cancelLinking: () => void
  setBranchMode: Dispatch<{ nodeId: string; mode: GraphNode['branchMode'] }>
  setBranches: Dispatch<{ nodeId: string; action: SetStateAction<string[]> }>
  setBranchConditions: Dispatch<{
    nodeId: string
    action: SetStateAction<BranchCondition[]>
  }>
  setBranchesScript: Dispatch<{ nodeId: string; script: string | undefined }>
  setBranchDecisionScript: Dispatch<{
    nodeId: string
    script: string | undefined
  }>
  setFallbackBranch: Dispatch<{
    nodeId: string
    branch: string | undefined
  }>
  toggleNodeFlag: Dispatch<{
    nodeId: string
    flag: 'isError' | 'isUpgrade' | 'isSkip' | 'isBlock'
  }>
  deleteNode: Dispatch<{ nodeId: string }>
  deleteEdge: Dispatch<{ from: string; fromBranch?: string; to: string }>
  promoteEdge: Dispatch<{ from: string; fromBranch?: string; to: string }>
}

export interface AppStore extends AppState, AppActions {}

const immer =
  <T extends State>(config: StateCreator<T>): StateCreator<T> =>
  (set, get, api) =>
    config(
      (partial, replace) => {
        const nextState =
          typeof partial === 'function'
            ? produce(partial as (state: T) => T)
            : (partial as T)
        return set(nextState, replace)
      },
      get,
      api
    )

const storeCreator: StateCreator<AppStore, SetState<AppStore>> = immer(
  (setSkipSnapshot, get) => {
    const set: SetState<AppStore> = (action) => {
      const before = get()
      setSkipSnapshot(action)
      const after = get()
      if (
        before.nodesById !== after.nodesById ||
        before.graph !== after.graph ||
        before.rootId !== after.rootId
      )
        setSkipSnapshot((s) => {
          s.history.push({
            nodesById: before.nodesById,
            graph: before.graph,
            rootId: before.rootId,
          })
          s.future = initialState.future
        })
    }

    return {
      ...initialState,

      undo: () => {
        setSkipSnapshot((s) => {
          const snap = s.history.pop()
          if (!snap) return
          s.future.push({
            nodesById: s.nodesById,
            graph: s.graph,
            rootId: s.rootId,
          })
          s.nodesById = snap.nodesById
          s.graph = snap.graph
          s.rootId = snap.rootId
        })
      },

      redo: () => {
        setSkipSnapshot((s) => {
          const snap = s.future.pop()
          if (!snap) return
          s.history.push({
            nodesById: s.nodesById,
            graph: s.graph,
            rootId: s.rootId,
          })
          s.nodesById = snap.nodesById
          s.graph = snap.graph
          s.rootId = snap.rootId
        })
      },

      reset: () => {
        set((s) => {
          s.nodesById = initialState.nodesById
          s.graph = initialState.graph
          s.rootId = initialState.rootId
          s.activation = initialState.activation
        })
      },

      setBranchRender: (payload) => {
        set((s) => {
          s.branchRender = payload
        })
      },

      setLayout: (payload) => {
        set((s) => {
          s.layout = payload
        })
      },

      setLayeredIterations: (payload) => {
        set((s) => {
          s.layeredIterations = payload
        })
      },

      setLayeredOption: ({ key, value }) => {
        set((s) => {
          s.layeredOptions[key] = value
        })
      },

      activate: (payload) => {
        set((s) => {
          s.activation = payload
        })
      },

      appendNode: ({ from, fromBranch, branchCount }) => {
        set((s) => {
          const id = genId()
          const node: GraphNode = {
            id,
            name: 'Node',
            branchMode: branchCount ? 'list' : 'none',
          }
          if (branchCount) {
            node.branches = greekAlphabet.slice(0, branchCount - 1)
            node.fallbackBranch = defaultFallbackBranch
            node.branchDecisionScript = defaultBranchDecisionScript
          }
          s.nodesById[id] = node
          s.graph[id] = []
          s.graph[from].push({
            from,
            fromBranch: fromBranch,
            to: id,
            type: 'tree',
          })
          s.activation = { type: 'node', nodeId: id }
        })
      },

      completeLinking: ({ to }) => {
        set((s) => {
          if (s.activation?.type !== 'linking') throw Error('Not linking')
          const { from, fromBranch } = s.activation
          const edges = s.graph[from]
          if (!edges.find((e) => e.fromBranch === fromBranch && e.to === to))
            edges.push({
              from,
              fromBranch: fromBranch,
              to,
              type: dagDfs(s.graph, to, from) ? 'cycle' : 'dag',
            })
          s.activation = null
        })
      },

      cancelLinking: () => {
        set((s) => {
          if (s.activation?.type !== 'linking') throw Error('Not linking')
          const { from: nodeId, fromBranch: branch } = s.activation
          s.activation =
            typeof branch === 'string'
              ? { type: 'branch', nodeId, branch }
              : { type: 'node', nodeId }
        })
      },

      setBranchMode: ({ nodeId, mode }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (mode === 'none') {
            delete node.branchDecisionScript
            delete node.branches
            delete node.branchConditions
            delete node.branchesScript
            delete node.fallbackBranch
            node.branchMode = mode
            return
          }
          let branches = computeBranches(node, false)
          if (!branches?.length) branches = ['alpha']
          if (!node.branchDecisionScript)
            node.branchDecisionScript = defaultBranchDecisionScript
          if (!node.fallbackBranch) node.fallbackBranch = defaultFallbackBranch
          switch (mode) {
            case 'list': {
              node.branches = branches
              delete node.branchConditions
              delete node.branchesScript
              break
            }
            case 'conditionList': {
              node.branchConditions = branches.map((name) => ({
                name,
                condition: `${node.branchDecisionScript} === '${name}'`,
              }))
              delete node.branches
              delete node.branchesScript
              delete node.branchDecisionScript
              break
            }
            case 'listScript': {
              node.branchesScript = `['${branches.join("','")}']`
              delete node.branches
              delete node.branchConditions
            }
          }
          node.branchMode = mode
        })
      },

      setBranches: ({ nodeId, action }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (typeof action === 'function')
            node.branches = action(node.branches || [])
          else node.branches = action
        })
      },

      setBranchConditions: ({ nodeId, action }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (typeof action === 'function')
            node.branchConditions = action(node.branchConditions || [])
          else node.branchConditions = action
        })
      },

      setBranchesScript: ({ nodeId, script }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (script === undefined) delete node.branchesScript
          else node.branchesScript = script
        })
      },

      setBranchDecisionScript: ({ nodeId, script }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (script === undefined) delete node.branchDecisionScript
          else node.branchDecisionScript = script
        })
      },

      setFallbackBranch: ({ nodeId, branch }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (branch === undefined) delete node.fallbackBranch
          else node.fallbackBranch = branch
        })
      },

      toggleNodeFlag: ({ nodeId, flag }) => {
        set((s) => {
          const node = s.nodesById[nodeId]
          if (node[flag]) delete node[flag]
          else node[flag] = true
        })
      },

      deleteNode: ({ nodeId }) => {
        set((s) => {
          const parentEdges = findEdges(s.graph, (e) => e.to === nodeId)
          const childEdges = s.graph[nodeId].filter((e) => e.to !== nodeId)
          const findBestEdge = (edges: GraphEdge[]) => {
            let best: GraphEdge | undefined
            for (const edge of edges) {
              if (edge.type === 'tree') return edge
              if (!best || (edge.type === 'dag' && best.type === 'cycle'))
                best = edge
            }
            return best
          }
          const bestParent = findBestEdge(parentEdges)?.from
          const bestChild = findBestEdge(childEdges)?.to
          delete s.nodesById[nodeId]
          delete s.graph[nodeId]
          s.activation = null
          const getType = (from: string, to: string) => {
            if (dagDfs(s.graph, to, from)) return 'cycle'
            if (s.graph[from].find((e) => e.type === 'tree')) return 'dag'
            return 'tree'
          }
          for (const edge of parentEdges) {
            const edges = s.graph[edge.from]
            edges.splice(edges.indexOf(edge), 1)
            if (bestChild && !edges.find((e) => e.to === bestChild)) {
              edge.to = bestChild
              edge.type = getType(edge.from, edge.to)
              edges.push(edge)
            }
          }
          if (bestParent) {
            const edges = s.graph[bestParent]
            for (const edge of childEdges) {
              if (!edges.find((e) => e.from === bestParent)) {
                edge.from = bestParent
                edge.type = getType(edge.from, edge.to)
                edges.push(edge)
              }
            }
          }
        })
      },

      deleteEdge: ({ from, fromBranch, to }) => {
        set((s) => {
          const edges = s.graph[from]
          const i = edges.findIndex(
            (e) => e.fromBranch === fromBranch && e.to === to
          )
          if (i < 0) throw Error('Edge not found')
          edges.splice(i, 1)
          promoteCycles(s.graph)
        })
      },

      promoteEdge: ({ from, fromBranch, to }) => {
        set((s) => {
          const promote = s.graph[from].find(
            (e) => e.fromBranch === fromBranch && e.to === to
          )
          if (!promote) throw Error('Edge not found')
          if (promote.type !== 'dag') throw Error('Edge must be type dag')
          const demote = findEdge(
            s.graph,
            (e) => e.type === 'tree' && e.to === to
          )
          if (!demote) throw Error('No tree edge sibling found')
          promote.type = 'tree'
          demote.type = 'dag'
        })
      },
    }
  }
)

export const useStore = create<AppStore>(
  persist(storeCreator, {
    name: 'graph-storage',
    blacklist: ['history', 'future'],
    version: 3,
    migrate: () => {
      console.warn(
        'Old app state storage version found. ' +
          'Migration not yet implemented. ' +
          'App state reset.'
      )
      return {} as AppStore
    },
  })
)
